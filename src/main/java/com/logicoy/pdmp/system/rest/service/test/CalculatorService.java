/**
 * Copyright (C) 2015 The Logicoy PDMP team (http://logicoy.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.logicoy.pdmp.system.rest.service.test;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Class Description : This class is just for the developer's reference, on how
 * to create a simple spring enabled rest services.
 *
 * @author Paramjeet Singh
 */
@Controller
@RequestMapping("/CalulatorService")
public class CalculatorService {

    private static Logger logger = Logger.getLogger(CalculatorService.class.getCanonicalName());

    /**
     * Default constructor
     */
    public CalculatorService() {

    }

    /**
     * Defines the rest method calculate, with request mapping /calculate This
     * method accepts the GET Method call from client, And return type in JSON
     * format.
     *
     * @param a Query parameters, Number 1
     * @param b Query parameters, Number 2
     * @param operation Defines the operation, which need to be performed in
     * given Numbers
     * @return JSON format of input, and output number
     */
    @RequestMapping(value = "/calculate", method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseBody
    public Map<String, String> calculate(String a, String b, String operation) {
        Map<String, String> map = new HashMap<>();
        
        map.put("input1", a);
        map.put("input2", b);
        
        double d = 0;
        Logger.getLogger("CalculatorService").log(Level.INFO, "a {0}", a);
        Logger.getLogger("CalculatorService").log(Level.INFO, "b {0}", b);
        Logger.getLogger("CalculatorService").log(Level.INFO, "Operation {0}", operation);
        if (operation.trim().equalsIgnoreCase("add")) {
            d = Double.parseDouble(a) + Double.parseDouble(b);
        } else if (operation.trim().equalsIgnoreCase("sub")) {
            d = Double.parseDouble(a) - Double.parseDouble(b);
        } else if (operation.trim().equalsIgnoreCase("mul")) {
            d = Double.parseDouble(a) * Double.parseDouble(b);
        } else if (operation.trim().equalsIgnoreCase("div")) {
            d = Double.parseDouble(a) / Double.parseDouble(b);
        }
        Logger.getLogger("CalculatorService").log(Level.INFO, "d {0}", d);
        map.put("result", String.valueOf(d));
        return map;
    }

}
