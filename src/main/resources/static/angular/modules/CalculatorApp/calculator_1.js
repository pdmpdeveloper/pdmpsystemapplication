/*
 * Copyright (C) 2015 The Logicoy PDMP team (http://logicoy.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
angular.module('CalculatorApp', [])
        .controller('CalculatorController', function ($scope, $http) {
            $scope.result = function () {

                var ur = "/PDMPSystemApp/rest/CalulatorService/calculate?a=" + $scope.a + "&b=" + $scope.b + "&operation=";

                if ($scope.operator == '+') {
                    ur += "add";
                }
                if ($scope.operator == '-') {
                    ur += "sub";
                }
                if ($scope.operator == '*') {
                    ur += "mul";
                }
                if ($scope.operator == '/') {
                    ur += "div";
                }

                var c = encodeURI(ur);

                $http.get(c).then(function (response) {
                    
                    console.log("Response message : " + response);
                    console.log("Response message : " + response.data);
                    console.log("Response message : " + response.data.result);
                    
                    $scope.c = response.data.result;
                    $scope.aR = response.data.input1;
                    $scope.bR = response.data.input2;
                    $scope.operatorR = $scope.operator;
                });

            };
            
            $scope.result1 = function () {

                

                if ($scope.operator == '+') {
                    return $scope.a + $scope.b;
                }
                if ($scope.operator == '-') {
                    return $scope.a - $scope.b;
                }
                if ($scope.operator == '*') {
                    return $scope.a * $scope.b;
                }
                if ($scope.operator == '/') {
                    return $scope.a / $scope.b;
                }

                

                

            };
        });
 